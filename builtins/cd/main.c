#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
//#include "main.h"

int	c_in_str_array(char c,char **tab)
{

  int i;
  int j;

  i = 0;
  j = 0;
  while (tab[i])
    {
      while (tab[i][j])
	{
	  if (tab[i][j] == c)
	    return 1;
	  j++;
	}
      i++;
    }
  
  return 0;

}

int	s_in_str_array(char *str,char **tab)
{
  int i;

  i = 0;
  while(tab[i]){
    if(strcmp(str,tab[i]))
      return 1;
    i++;
  }

  return 0;
}

int count(char **tab)
{
  int	i;

  i = 0;
  while(tab[i])
    i++;
  return i;
}

int my_putstr(char *str)
{
  int i;

  i = 0;
  while (str[i])
    {
      write(1, &str[i], 1);
      i++;
    }
  return i;
}

int cd(char *pathname)
{
  struct stat sb;
  char *newpath;
  
  if (strlen(pathname) == 1 && pathname[0] == '~')
    {
      // RETOUR DE MALLOC PAS VERIFIE SHEITAN
      newpath = malloc(sizeof(char) * strlen(getenv("HOME")));
      newpath = getenv("HOME");
    }
  else if (strlen(pathname) == 1 && pathname[0] == '-')
    {
      newpath = malloc(sizeof(char) * strlen(getenv("OLDPWD")));
      newpath = getenv("OLDPWD");
    }
  else
    newpath = pathname;

  if (stat(newpath, &sb) == -1)
    {
      my_putstr("cd :");
      my_putstr(newpath);
      my_putstr(": no such file or directory\n");
      exit(EXIT_FAILURE);
    }
  if((sb.st_mode & S_IFMT) == S_IFDIR)
    {
      if(chdir(newpath) == 0)
	{
	  printf("new dir :%s\n", newpath);
	  return 1;
	}
      else
	my_putstr("chdir failed \n");
    }
  else
    {
      my_putstr("cd :");
      my_putstr(newpath);
      my_putstr(": not a directory\n");
      exit(EXIT_FAILURE);
    }
}

int main(int ac, char**av, char** envp)
{
  if(ac > 1)
    cd(av[1]);
  else
    cd("~\0");
  return 0;
}
