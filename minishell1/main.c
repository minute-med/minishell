#include "main.h"
#include "cd.h"
#include "print.h"
#include "exit.h"

int count(char **tab)
{
  int	i;

  i = 0;
  while(tab[i])
    {      
      i++;
    }
  return i;
}

int is_builtin(char *cmd)
{

  if(my_strcmp(cmd, "cd"))
    return 1;
  else if(my_strcmp(cmd, "env"))
    return 1;
  else if(my_strcmp(cmd, "setenv"))
    return 0;
  else if(my_strcmp(cmd, "unsetenv"))
    return 0;
  else if(my_strcmp(cmd, "exit"))
    return 1;
  else
    return 0;
}

int call_builtin(t_env * data, char *cmd, char **args)
{
  if(my_strcmp(cmd, "cd"))
    {
      if(count(args) < 2)
	return cd(data, /*(char *)*/0);
      else
	return cd(data, args[1]);
      
    }
  else if(my_strcmp(cmd, "env"))
    {
      my_env(data);
    }
  else if(my_strcmp(cmd, "setenv"))
    return 0;
  else if(my_strcmp(cmd, "unsetenv"))
    return 0;
  else if(my_strcmp(cmd, "exit"))
    my_exit();
  else
    return 0;

}

int my_exec(t_env *data, char *binary, char **args)
{
  char *cmd;
  int status;

  cmd = NULL;

  if((cmd = check_path(my_getenv(data, "PATH"), binary)))
    {
      if (fork() == 0)
	execve(cmd, args, data->envp);
      else
	wait(&status);
    }
  else
    {
      not_found_error(binary);
      return 0;
    }
  return 1;
}

int prompt(t_env *data, char **argv)
{
  
  char	 *buff;
  char	*cleancmd;
  char	**opts;
  char	**args;
  
  if((buff = malloc(4096 + 1)) == 0)
    return 1;

  while(42){
    memset(buff, 0, 4096);
    print_prompt(data);
    if(read(0, buff, 4096) < 0)
      my_putstr("could not read\n");
    else
      {
	buff[my_strlen(buff) - 1] = 0;
	opts = explode(' ', buff);
	if(count(opts) == 1)
	  {
	    
	    cleancmd = opts[0];
	    args = argv;
	  }
	else
	  {
	    cleancmd = opts[0];
	    args = opts;
	  }
	if(is_builtin(cleancmd))
	  call_builtin(data, cleancmd, args);
	else
	  my_exec(data, cleancmd, args);
      }
  }
}


t_env *init_env(char **envp)
{
  
  int		i;
  t_env		*data;

  if((data = malloc(sizeof(t_env))) == 0)
    {
      my_putstr("environement memory allocation failed\n");
      exit(EXIT_FAILURE);
      return data;
    }
  
  if((data->env = malloc(sizeof(map**) * (count(envp) + 1))) == 0)
    {
      my_putstr("environement memory allocation failed\n");
      exit(EXIT_FAILURE);
      return data;
    }
  
  if((data->envp = malloc(sizeof(char**) * (count(envp) + 1))) == 0)
    {
      my_putstr("environement memory allocation failed\n");
      exit(EXIT_FAILURE);
      return data;
    }
  
  i = 0;
  while(envp[i])
    {
      char ** envPair = NULL;
      envPair = split('=', envp[i]);
      if((data->env[i] = malloc(sizeof(map))) == 0 )
	{
	  exit(EXIT_FAILURE);
	  return NULL;
	}
      data->env[i]->key =  envPair[0];
      data->env[i]->value =  envPair[1];
      
      data->envp[i] = envp[i];

      if (my_strncmp("PATH", envPair[0], my_strlen(envPair[0])))
	{
	  data->path = explode(':', envPair[1]);
	}
      i++;
    }  
  data->envp[i] = 0;
  data->env[i] = 0;
  return data;
}

int main(int argc, char **argv, char**envp){

  t_env *data = init_env(envp);
  prompt(data, argv);

  return (0);
}
