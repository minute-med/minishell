#include "print.h"

void print_str_array(char **tab)
{

  int	i;
  i =0;
  while(tab[i])
    {      
      my_putstr(tab[i]);
      my_putstr("$\n");
      i++;
    }
}

void not_found_error(char *cmd)
{
  my_putstr(cmd);
  my_putstr(": command not found\n");
}

void print_prompt(t_env *data)
{
  char *logname;
  char *path;

  /* printf("PWD(out if):%s\n", my_getenv(data, "PWD")); */

  logname = my_getenv(data, "LOGNAME");
  if(my_strncmp(my_getenv(data, "HOME"), my_getenv(data, "PWD"), my_strlen(my_getenv(data, "HOME"))))
    {
      int n = my_strlen(my_getenv(data, "HOME"));
      int m = my_strlen(my_getenv(data, "PWD"));
      
      /*
	printf("PWD:%s\n", my_getenv(data, "PWD"));
      printf("n:%d\nm:%d\n", n, m);
      printf("truc a coller a ~:%s\n",strnmcpy(my_getenv(data, "PWD"), n, m));
       */
      path = my_strcatdup("~", strnmcpy(my_getenv(data, "PWD"), n, m));
    }
  else
    path = my_getenv(data, "PWD");

  my_putstr(logname);
  my_putstr(":\0");
  my_putstr(path);
  my_putstr("$>");

}
