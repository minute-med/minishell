#include "main.h"

/*
char *get_shell_env(char *varname)
{
  int n;
  
  extern char **environ;
  char **env;

  env = environ;
  n   = my_strlen(varname);
  for (; *env != 0; env++)
    {
      if(my_strncmp(*env, varname, n))
	return *(env) + (n +1);
    }
  return NULL;
}

 */

char *my_getenv(t_env *data, char *varname)
{
  int i;

  i = 0;
  while(data->env[i])
    {
      if(my_strcmp(data->env[i]->key, varname))
	return data->env[i]->value;
      i++;
    }
  return NULL;
}

char *my_dupenv(t_env *data, char *varname)
{
int i;

  i = 0;
  while(data->env[i])
    {
      if(my_strcmp(data->env[i]->key, varname))
	return my_strcatdup("", data->env[i]->value);
      i++;
    }
  return NULL;
}

int my_setenv(t_env *data,char *key, char *value, int overwrite)
{
  int	i;
  int	j;

  i = 0;
  j = 0;

  while(data->env[i])
    {
      if(my_strcmp(data->env[i]->key, key) && overwrite)
	{
	  free(data->env[i]->value);
	  if((data->env[i]->value = malloc(sizeof(char) * my_strlen(value) + 1)) == 0)
	      return 0;
	  while(value[j])
	    {
	      data->env[i]->value[j] = value[j];
	      j++;
	    }
	  data->env[i]->value[j] = 0;
	  return 1;
	}
      i++;
    }
  return 0;
}

int my_unsetenv(char *key)
{
  return 0;
}

int my_env(t_env *data)
{
  int i;

  i = 0;

  while(data->env[i])
    {
      my_putstr(data->env[i]->key);
      my_putstr("=");
      my_putstr(data->env[i]->value);
      my_putstr("\n");
      i++;
    }

  return 1;
}

