#ifndef _ENV_H
#define _ENV_H

char *my_getenv(t_env *data, char *varname);
char *my_dupenv(t_env *data, char *varname);
int my_setenv(t_env *data, char *key, char *value, int overwrite);
int my_unsetenv(char *key);
int my_env(t_env *data);

#endif /* _ENV_H */
