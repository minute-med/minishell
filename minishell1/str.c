#include "str.h"

void my_putstr(char *str)
{
  int i;
  i = 0;

  while (str[i])
    {
      write(1, &str[i], 1);
      i++;
    }
}

int my_strlen(char *str)
{
  int i;
  i = 0;
  while (str[i] != 0)
      i++;
  return i;
}

char*epur_str(char *str)
{
  int i;
  int j;

  i = -1;
  j = 0;
  if (!str)
    return (0);
  while (str[++i] != '\0')
    {
      if (str[i] != '\n' && str[i] != '\r' && str[i] != '\t')
        {
          str[j] = str[i];
          j++;
          if (str[i] == '\n' || str[i] == '\r' || str[i] == '\t')
            {
              str[j] = ' ';
              j++;
            }
        }
    }
  str[j+ 1] = '\0';
  if (str[j - 1] == ' ')
    str[j - 1] = '\0';
  return (str);
}

int my_strcmp(char *str1, char *str2)
{
  int i; 
  i = 0;

  while (str1[i])
    {
      if (str1[i] != str2[i])
	return 0;
      i++;
    }
  return 1;
}

int my_strncmp(char *str1, char *str2,int n)
{
  
  int i;
  
  i = 0;
  
  while (i < n)
    {
      if (str1[i] != str2[i])
	return 0;
      i++;
    }
  return 1;
}

char *strnmcpy(char *str,int n, int m)
{
  char *newstr = NULL;
  int i;
  int j;
  int a;
  int b;

  if (n < m)
    {
      a = n;
      b = m;
    }
  else if(n > m)
    {
      a = m;
      b = n;
    }
  else
    {
      a = n;
      b = m;
    }
  if(n < 0 || m < 0)
    return 0;
    
  if(my_strlen(str) < n || my_strlen(str) < m)
    {
      my_putstr("my_strlen(str) < n || my_strlen(str) < m : ERREUR\n");
      return (0);
    }

  if((newstr = malloc(sizeof(char)*((b - a) + 2))) == 0)
    {
      my_putstr("malloc ERREUR\n");
      return NULL;
    }

  i = 0;
  j = 0;

  while(str[i] != 0)
    {
      if(i >= a && i <= b)
	{
	  newstr[j] = str[i];
	  j++;
	}
      i++;
    }
  newstr[j] = '\0';
  return (newstr);
}

char *implode(char delim, char **tab)
{
  int	i;
  int	j;
  int	len;
  int	finalstrlen;
  char	*str;

  i = 0;
  j = 0;
  len = 0;
  while(tab[i])
    {
      len += my_strlen(tab[i]);
      if(delim != 0)
	len++;
      i++;
    }
  finalstrlen = len;

  if((str = malloc(sizeof(char) * (len + 1))) == 0)
    return NULL;
  i = 0;
  len = 0;
  while(tab[i])
    {
      j = 0;
      while(tab[i][j])
	{
	  str[len] = tab[i][j];
	  j++;
	  len++;
	}

      if(delim != 0 && len != 0 && len != finalstrlen - 1)
	 {
	   str[len] = delim;
	   len++;
	 }
      i++;
    }
  str[len] = 0;
  return (str);
}


char **explode(char delimiter, char *str)
{
  int	i;
  int	j;
  int	c;

  char **tab;
  
  i = 0;
  c = 1;

  while (str[i])
    {
      if(str[i] == delimiter)
	{
	  c++;
	}
      i++;
    }
  
  if((tab = malloc(sizeof(char*) * (c + 1))) == 0)
    return NULL;

  i = 0;
  j = 0;
  c = 0;
  while (str[i])
    {
      if(str[i] == delimiter)
	{
	  if ((tab[j] = strnmcpy(str, c, i - 1)) != 0)
	    j++;
	  c = i + 1;
	}
      i++;
    }
  
  if(str[c] != 0)
    tab[j] = strnmcpy(str, c, i);
  tab[j + 1] = 0;
  return tab;
}

char **split(char delimiter, char *str)
{
  int	i;
  int	j;
  int	c;

  char **tab;

  i = 0;
  c = 0;
  j = 0;
  while (str[i])
    {
      if(str[i] == delimiter)
	{
	  c = 1;
	  j = i;
	  break;
	}
      i++;
    }
  if(c)
    {
      if((tab = malloc(sizeof(char**) * 2 + 1)) == 0)
	{
	  return tab;
	}
      
      tab[0] = strnmcpy(str, 0, j - 1);
      if(j + 1 <= my_strlen(str))
	{
	  tab[1] = strnmcpy(str, j + 1, my_strlen(str));
	}
      else
	return NULL;
    }
  else
    return NULL;
  return tab;
}

char *my_strcatdup(char *dest, char *src)
{
  int	i;
  int	j;
  int	dest_len;
  int	src_len;
  char	*newdest;

  i = 0;
  j = 0;
  dest_len = my_strlen(dest);
  src_len = my_strlen(src);

  if((newdest = malloc(sizeof(char) * (dest_len + src_len) + 1)) == 0)
    return NULL;
  while (dest[i])
    {
      newdest[i] = dest[i];
      i++;
    }
  while(src[j])
    {
      newdest[i] = src[j];
      j++;
      i++;
    }
  newdest[i] = '\0';
  return newdest;
}


/*
  function that remove '/n' '/t' and '/r' characters
  char* str		# string to epur
  @bool charsonly	# bool to say if you want to remove all exept				a-z
 */

int  is_alpha_char(char c)
{
      if(c < 65 && c > 90 && c < 97 && c > 122)
	return 0;
      else
	return 1;
}


/*
char *epur_str(char *str, int charsonly)
{
  
  int	i;
  int	j;

  i = 0;
  i = 0;
  while(str[i])
    {
    if(str[i] == '\n' || str[i] == '\r' || str[i] == '\t' 
	 || (charsonly && is_alpha_char(str[i]) == 0))
	{
	if(str[i + 1] )
	    str[i] = 
	}
      else
	{
	  // bon char
	}
      i++;
    }
}
*/
