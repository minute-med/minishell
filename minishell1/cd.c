#include "cd.h"
#include "str.h"
int	c_in_str_array(char c,char **tab)
{

  int i;
  int j;

  i = 0;
  j = 0;
  while (tab[i])
    {
      while (tab[i][j])
	{
	  if (tab[i][j] == c)
	    return 1;
	  j++;
	}
      i++;
    }
  
  return 0;

}

int	s_in_str_array(char *str,char **tab)
{
  int i;

  i = 0;
  while(tab[i]){
    if(strcmp(str,tab[i]))
      return 1;
    i++;
  }

  return 0;
}

void	print_isdir_error(int code, char *pathname)
{
  if(code > 0)
    {
      my_putstr("cd :");
      my_putstr(pathname);
      my_putstr(": not a directory\n");
    }
  else
    {
      my_putstr("cd :");
      my_putstr(pathname);
      my_putstr(": no such file or directory\n");
    }
}

int	is_dir(char *pathname)
{
  struct stat sb;

  if (stat(pathname, &sb) == -1)
    return -1;
  if((sb.st_mode & S_IFMT) == S_IFDIR)
    return 0;
  else
    return 1;
}

int	cd(t_env * data, char *pathname)
{

  char		**tmpSplited;
  char		*newpath;
  int		dircode;
  
  dircode = -1;

  if(pathname == 0)
    newpath = my_dupenv(data,"HOME");
  else
    {
      if (strlen(pathname) == 1 && pathname[0] == '~')
	newpath = my_dupenv(data, "HOME");
      else if (strlen(pathname) == 1 && pathname[0] == '-')
	newpath = my_dupenv(data, "OLDPWD");
      else if (strlen(pathname) == 1 && pathname[0] == '.')
	newpath = my_dupenv(data, "PWD");
      else if (strlen(pathname) == 2 && my_strcmp("..", pathname))
	{
	  tmpSplited = explode('/', my_getenv(data,"PWD"));
	  tmpSplited[count(tmpSplited) -1] = 0;
	  newpath = implode('/', tmpSplited);
	  newpath = my_strcatdup("/", newpath);
	}
      
      else
	{
	  if(pathname[0] == '/')   
	    newpath = pathname;
	  else
	    {
	      newpath = my_strcatdup(my_getenv(data, "PWD"),"/");
	      newpath = my_strcatdup(newpath, pathname);
	    }
	  
	}
    }
  
  printf("cd() newpath :%s\n",newpath);
 if((dircode = is_dir(newpath)) == 0)
   {
     if(chdir(newpath) == 0)
       {
	 
	 my_setenv(data , "OLDPWD", my_getenv(data, "PWD"), 1);
	 my_setenv(data , "PWD", newpath, 1);
	 return 1;
	 }
      else
	{
	  my_putstr("chdir failed \n");
	  return 0;
	}
   }
 else
   print_isdir_error(dircode, newpath);
 return 0;
}
