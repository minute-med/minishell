#ifndef _MAIN_H
#define _MAIN_H

#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "types.h"
#include "tools.h"
#include "str.h"
#include "env.h"


int count(char **tab);
int is_builtin(char *cmd);
int call_builtin(t_env * data,char *cmd, char **args);
int my_exec(t_env *data, char *binary, char **args);
int prompt(t_env *data, char **argv);
t_env *init_env(char **envp);

#endif /* _MAIN_H */
