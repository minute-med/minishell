#ifndef _TYPES_H
#define _TYPES_H


typedef struct t_map{

  char	*key;
  char	*value;

} map;

typedef struct t_data {

  map	**env;
  char	**envp;
  char	**path;

} t_env;



#endif /* _TYPES_H */
