#include "tools.h"
#include "str.h"


char *check_path(char *path, char *cmd)
{
  char	**paths;
  int	i;
  
  paths = explode(':', path);
  i = 0;
  /*
    verifier si la comande est un builtins
  */
  while(paths[i])
    {
      char *tmpPath = my_strcatdup(my_strcatdup(paths[i],"/\0"), cmd);
      /*printf("tmppath:%s\n", tmpPath);*/
      if(access(tmpPath, F_OK) == 0 && access(tmpPath, X_OK) == 0)
	return tmpPath;
      i++;
    }
  i = 0;
  
  return NULL;
}
