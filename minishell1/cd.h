#ifndef _CD_H
#define _CD_H

#include "main.h"
#include "env.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int	c_in_str_array(char c,char **tab);
int	s_in_str_array(char *str,char **tab);
void	print_isdir_error(int code, char *pathname);
int	is_dir(char *pathname);
int	cd(t_env * data, char *pathname);
#endif /*_CD_H */
