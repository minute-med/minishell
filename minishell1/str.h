#ifndef _STR_H
#define _STR_H

#include <stdlib.h>
#include <unistd.h>

void	my_putstr(char *str);
int	my_strlen(char *str);
char	*epur_str(char *str); /* recoder */
int	my_strcmp(char *str1, char *str2);
int	my_strncmp(char *str1, char *str2,int n);
char	*strnmcpy(char *str,int n, int m);
char	*implode(char delimiter, char **tab);
char	**explode(char delimiter, char *str);
char	**split(char delimiter, char *str);
char	*my_strcatdup(char *dest, char *src);
int	is_alpha_char(char c);
	
#endif /* _STR_H */
