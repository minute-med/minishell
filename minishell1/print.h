#ifndef _PRINT_H
#define _PRINT_H

#include "main.h"
#include "env.h"

void print_str_array(char **tab);
void not_found_error(char *cmd);
void print_prompt(t_env *data);

#endif /* _PRINT_H */
